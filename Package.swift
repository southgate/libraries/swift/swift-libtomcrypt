// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "libtomcrypt",
    products: [
        .library(
            name: "libtomcrypt",
            targets: ["libtomcrypt"]),
    ],
    dependencies: [],
    targets: [
        .target(
            name: "libtomcrypt",
            dependencies: [],
            publicHeadersPath: "headers",
            cSettings: [
                .define("LTC_SOURCE"),
                .headerSearchPath("headers")
            ]
        ),
    ]
)
